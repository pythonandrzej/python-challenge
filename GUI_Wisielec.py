word_to_be_drawn = ["małpa", "żyrandol", "kabina", "stolik", "żyrafa", "łóżko", "majeranek", "tenis", "przedramię", "pomarańcza", "kaloryfer",\
                     "kangur", "chrząszcz", "herbatnik", "safari", "zajączek", "majonez", "bieganie", "rozrusznik", "akumulator"]
import tkinter as tk
import random
import time

figures = ["","""|     
|    
|""", """|
|
|    
|     
|    
|""", """___
|
|        
|     
|    
|""", """______
|
|        
|     
|    
|""", """______
|          |
|          
|     
|    
|""", """______
|          |
|          |    
|     
|    
|""","""______ 
|          |
|          |    
|         O
|    
|""","""______ 
|          |
|          |    
|         O
|          |
|""","""______ 
|          |
|          |    
|         O
|          |/
|""","""______ 
|          |
|          |    
|         O
|         \|/
|""","""______ 
|          |
|          |    
|         O
|         \|/
|         /""","""______ 
|          |
|          |    
|         O
|         \|/
|         / \ KONIEC GRY! PRZEGRAŁEŚ"""]

password_to_show = []
password = random.choice(word_to_be_drawn)
text_to_show = figures[0]


def game():
    txt_hangman.configure(state="normal")
    letter_guessed = False
    password_to_display=[]
    current_displayed_password = lbl_password["text"]
    for i in current_displayed_password:
        password_to_display.append(i)
    z = -1
    letter = ent_frame_provide_letter1.get()
    for i in password:
        z = z + 1
        if letter == i:
            password_to_display[z] = letter
            letter_guessed = True 
        else:
            pass
    if letter_guessed == True:
        text_to_show = txt_hangman.get("1.0", tk.END)
    else:
        text_to_show = txt_hangman.get("1.0", tk.END)
        y = -1
        for i in figures:
            y = y + 1
            if text_to_show.strip() == i:
                text_to_show = figures[y+1]
                break
            else:
                pass
    password_to_display = "".join(password_to_display)
    txt_hangman.delete("1.0",tk.END)
    txt_hangman.insert("1.0",f"{text_to_show}")
    txt_hangman.pack()
    txt_hangman.configure(state='disabled')
    lbl_password["text"]= f"{password_to_display}"
    if lbl_password["text"] == password:
        lbl_password["text"] = lbl_password["text"] + "     HASŁO ODGADNIĘTE. WYGRAŁEŚ!!!"
    return 

window = tk.Tk()
window.wm_attributes('-alpha', 0.7) #window manager atributes making it tranparent - level of tranparency is the second argument
window.geometry("600x400")
window.title("ZAGRAJMY W WISIELCA. TY ODGADUJESZ HASLO WYMYŚLONE PRZEZE MNIE. POWODZENIA")
window.grid()

frame_password = tk.Frame(window,relief = tk.SUNKEN, borderwidth = 5)
lbl_password = tk. Label(frame_password, text = f"{len(password)*'*'}", font ="20")
lbl_password.pack()
frame_password.pack(fill = tk.BOTH)

frame_provide_letter = tk.Frame(window,relief = tk.SUNKEN, borderwidth = 5)
lbl_frame_provide_letter = tk. Label(frame_provide_letter, text = f"PODAJ LITERĘ I KLIKNIIJ 'OK':", font ="20")
lbl_frame_provide_letter.pack()
frame_provide_letter.pack(side = tk.LEFT, fill = tk.BOTH)

frame_provide_letter1 = tk.Frame(window,relief = tk.SUNKEN, borderwidth = 5)
ent_frame_provide_letter1 = tk.Entry(frame_provide_letter1, font ="20")
ent_frame_provide_letter1.pack(side = tk.LEFT)
frame_provide_letter1.pack(fill = tk.BOTH)
btn_OK = tk.Button(frame_provide_letter1, text = "OK", bg = "#34A2FE", width = "90", command = game)
btn_OK.pack()

frame_hangman = tk.Frame(window,relief = tk.SUNKEN, borderwidth = 5)
txt_hangman = tk.Text(frame_hangman, font = "40")
frame_hangman.pack(fill = tk.BOTH)
txt_hangman.insert("1.0",f"{text_to_show}")
txt_hangman.pack()

window.mainloop()
