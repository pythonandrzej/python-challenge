from pathlib import Path
import PyPDF2 


class PdfFileSplitter:
   
    def __init__(self, pdf_path):
        self.pdf_reader = PyPDF2.PdfFileReader(pdf_path)
        
    def split(self, breakpoint):
        self.writer1 = PyPDF2.PdfFileWriter()
        self.writer2 = PyPDF2.PdfFileWriter()
        
        for page in self.pdf_reader.pages[:breakpoint]:
            self.writer1.addPage(page)
      
        for page in self.pdf_reader.pages[breakpoint:]:
            self.writer2.addPage(page)

    def write(self, filename):
       
        with Path(filename + "_1.pdf").open(mode="wb") as output_file:
            self.writer1.write(output_file)
     
        with Path(filename + "_2.pdf").open(mode="wb") as output_file:
            self.writer2.write(output_file)

pdf_to_be_split = PdfFileSplitter("regulamin.pdf")
pdf_to_be_split.split(breakpoint=2)
pdf_to_be_split.write("regulamin_split")
