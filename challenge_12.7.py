from pathlib import Path
import csv

src = list()
dest = list()

src_path = Path.home() / "Desktop/Python proba/scores.csv" 
dest_path = Path.home() / "Desktop/Python proba/highest_scores.csv" 

with src_path.open(mode = "r", encoding = "utf-8", newline = "") as table_of_scores:
    table_of_scores = csv.reader(table_of_scores)
    for a in table_of_scores:
        src.append([value for value in a])
    table_dict = csv.DictReader(table_of_scores)

src.pop(-1) 
header = src.pop(0) 
src.sort() 

for f in range(1, len(src)): 
    if src[f][0] == src[f-1][0]:
      if int(src[f-1][1]) < int(src[f][1]): #do wyniku mniejszego niż maksymalny -zamianiam nazwisko na "do kasacji"
       src[f-1][0] = "do kasacji"
    else:
      pass
    
src.insert(0, header) 

for f in range(len(src)): #zostawiam w liście tylko pozycje z maksymalnymi wynikami dla danego gracza
    if src[f][0] == "do kasacji":
        pass
    else:
        dest.append(src[f]) 

with dest_path.open(mode = "w", encoding = "utf-8", newline="") as table_of_scores:
    table_of_scores = csv.writer(table_of_scores)
    for j in range(len(dest)):
        table_of_scores.writerow(dest[j])
