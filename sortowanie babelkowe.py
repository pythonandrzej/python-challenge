
def bubble_sort(number_to_sort):
    number_bubbled = list(number_to_sort)
    for i in range(len(number_to_sort)):
        for i in range(len(number_to_sort) - 1):
            if number_bubbled[i] > number_bubbled[i + 1]:
                a = number_bubbled[i]
                number_bubbled[i] = number_bubbled[i + 1]
                number_bubbled[i + 1] = a
    return number_bubbled


