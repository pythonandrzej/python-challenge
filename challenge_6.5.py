def invest(amount, rate, years):
    for n in range(1, 1 + years):
        amount = amount * (rate + 1)
        print(f" year {n}:   ${amount:.2f}")

invest(float(input("Podaj kwotę początkową: ")), float(input("Podaj roczną stopę procentową: ")), int(input("Podaj liczbę lat na jaką inwestujesz: ")))

