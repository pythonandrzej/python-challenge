from pathlib import Path
import PyPDF2 


def get_page_text(page):
    return page.extractText()

pdf_path = Path.home() / "Desktop/scrambled.pdf"

pdf_reader = PyPDF2.PdfFileReader(str(pdf_path))
pdf_writer = PyPDF2.PdfFileWriter()

pages = list(pdf_reader.pages)
pages.sort(key=get_page_text)

for page in pages:
    rotation_degrees = page["/Rotate"]
    if rotation_degrees != 0:
        page.rotateCounterClockwise(rotation_degrees)
    pdf_writer.addPage(page)

output_path = Path.home() / "unscrambled.pdf"
with output_path.open(mode="wb") as output_file:
    pdf_writer.write(output_file)

