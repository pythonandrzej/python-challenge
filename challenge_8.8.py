import random

number_of_flips_finished = 0
sequence = ""
a = 0

for i in range(10_000):
    flip = random.randint(0,1)
    sequence = sequence + str(flip) #program tworzy string by porównywać wyniki kolejnych rzutów
                                    #pomiędzy sobą
    
    if sequence[i] != sequence[i-1] and (i - a) != 1: #warunek ten zapobiega policzeniu jako nowej sekwencji sytuacji
                                                      #kiedy wypadnie zaraz po zakończeniu jednej, kolejny rzut da wynik inny , niż
                                                      # ostatni rzut sekcwencji poprzedniej tj.: by np.: reszka/orzeł/reszka/orzeł/reszka/orzeł/reszka
                                                      #policzyć jako 3 a nie 6 sekwencji
        number_of_flips_finished = number_of_flips_finished + 1
        a = i
                
avg_number_flip_trial = 10_000 / number_of_flips_finished 
print (avg_number_flip_trial)
