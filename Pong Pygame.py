import pygame
import win32api
import win32con
import win32gui
from sys import exit

pygame.init()
screen = pygame.display.set_mode((1000,700))
pygame.display.set_caption("PONG GAME")
color = (255, 0, 128)
ball = pygame.Rect(500,120,30,30)
paddle_player_top = pygame.Rect(40,40,30,20)
paddle_player_center = pygame.Rect(40,60,30,60)
paddle_player_bottom = pygame.Rect(40,120,30,20)
paddle_computer_top = pygame.Rect(920,40,30,20)
paddle_computer_center = pygame.Rect(920,60,30,60)
paddle_computer_bottom = pygame.Rect(920,120,30,20)
x_switch = -2
y_switch = 1
score = [0,0]
touch_point_with_computer_paddle = 0
paddle_computer_center_final = None
pygame.font.init() 
result_font = pygame.font.SysFont('Comic Sans MS', 80)
computer_touched_last = True
player_touched_last = True
        
def draw_elements():
    result_player = result_font.render(str(score[0]),False,"white")
    result_computer = result_font.render(str(score[1]),False,"white")
    screen.fill(color)
    screen.blit(result_player,(200,0))
    screen.blit(result_computer,(700,0))
    pygame.draw.rect(screen, ("white"), paddle_player_top)
    pygame.draw.rect(screen, ("white"), paddle_player_center)
    pygame.draw.rect(screen, ("white"), paddle_player_bottom)
    pygame.draw.rect(screen, ("white"), paddle_computer_top)
    pygame.draw.rect(screen, ("white"), paddle_computer_center)
    pygame.draw.rect(screen, ("white"), paddle_computer_bottom)
    pygame.draw.ellipse(screen, ("white"), ball)
  
def ball_position_calculation(): #calculates where the computer paddle need to move, to bounce the ball
    ball_position = int(ball.y)
    number_of_moves = 852/int(x_switch)
    direction = y_switch
    for i in range(int(number_of_moves)):
        ball_position += 1*direction
        if ball_position >= 670 or ball_position <= 0: #change the calulation of ball final position when ball touches the bottom or top
            direction *= -1
    return ball_position

# Create layered window
hwnd = pygame.display.get_wm_info()["window"]
win32gui.SetWindowLong(hwnd, win32con.GWL_EXSTYLE,win32gui.GetWindowLong(hwnd, win32con.GWL_EXSTYLE) | win32con.WS_EX_LAYERED)
# Set window transparency color
win32gui.SetLayeredWindowAttributes(hwnd, win32api.RGB(*color), 0, win32con.LWA_COLORKEY)
clock = pygame.time.Clock()

while 1 == 1:
    
    draw_elements()
    previous_ball_position_x = ball.x
    previous_ball_position_y = ball.y
    
    for event in pygame.event.get():
        if event.type == pygame.QUIT:
            pygame.quit()
            exit()
    ball.x += x_switch
    ball.y += y_switch
        
    if ball.bottom >= 700 or ball.top <= 0: #bounce the ball from the top and the bottom of screen
        y_switch *= -1
    # computer_touched_last - allows to count only first touched between object. 
    if paddle_player_center.colliderect(ball) and computer_touched_last == True: #after bounce from the middle of the paddle ball accelerates with gentle angle
        if x_switch < 6: x_switch *= -2
        else: x_switch *= -1
        paddle_computer_center_final = True
        touch_point_with_computer_paddle = ball_position_calculation()
        computer_touched_last = False
        player_touched_last = True
        
    if computer_touched_last == True and any([paddle_player_top.colliderect(ball) or paddle_player_bottom.colliderect(ball)]) : #fter bounce from the middle of the paddle ball accelerates with harder angle
       x_switch *= -1
       if x_switch < 6: y_switch *= -2
       else: y_switch *= -1
       paddle_computer_center_final = True
       touch_point_with_computer_paddle = ball_position_calculation()
       computer_touched_last = False
       player_touched_last = True

    if paddle_computer_center_final == True: #moves the computer paddle as an reaction for the player hit 
        if abs(touch_point_with_computer_paddle) > paddle_computer_center.y:
            paddle_computer_center.y += 2
            paddle_computer_top.y +=2
            paddle_computer_bottom.y +=2
        else:
            paddle_computer_center.y -=2
            paddle_computer_top.y -=2
            paddle_computer_bottom.y -=2
    
    if paddle_computer_center.y>abs(touch_point_with_computer_paddle)-5 and paddle_computer_center.y < abs(touch_point_with_computer_paddle)+5: #stops the move of computer paddle when predicted position acheivedd
        paddle_computer_center_final = None
         
    if paddle_computer_center.colliderect(ball) and player_touched_last == True: #hit by computer paddle center
        if x_switch < 6: x_switch *= -2
        else: x_switch *= -1
        computer_touched_last = True
        player_touched_last = False
        
    if  player_touched_last == True and any([paddle_computer_top.colliderect(ball) or paddle_computer_bottom.colliderect(ball)]) : #hit by computer paddle top,bottom
        x_switch *= -1
        if y_switch < 6: y_switch *= -2
        else: y_switch *= -1
        computer_touched_last = True 
        player_touched_last = False

    if ball.right >= 1000: 
        x_switch = -2
        y_switch = 1
        ball = pygame.Rect(500,120,30,30)
        score[0] += 1
        computer_touched_last = True
        player_touched_last = True
    if ball.right <= 0: 
        x_switch = -2
        y_switch = 1
        ball = pygame.Rect(500,120,30,30)
        score[1] += 1
        computer_touched_last = True
        player_touched_last = True
        
    if score[1] == 10 or score[0] == 10:
        x_switch = 0
        y_switch = 0
        if score[0] == 10:
            winner = "WYGRAŁEŚ!!!"
        else:
            winner = "PRZEGRAŁEŚ!!!"
        final_text = result_font.render(f"Koniec gry.{winner}",False,"white")
        screen.blit(final_text,(50,200))
        final_text_decision = result_font.render(f"Wciśni spację, by zagrać",False,"white")
        screen.blit(final_text_decision,(10,250))
        pygame.key.get_pressed()
        if arrow[pygame.K_SPACE]:
            score= [0,0]
            x_switch = -2
            y_switch = 1

    arrow = pygame.key.get_pressed() #players' paddle moves when arrow up or down on the keyboard pressed 
    if arrow[pygame.K_UP]:
        paddle_player_top.y -=5
        paddle_player_center.y -=5
        paddle_player_bottom.y -=5
    if arrow[pygame.K_DOWN]:
        paddle_player_top.y +=5
        paddle_player_center.y +=5
        paddle_player_bottom.y +=5   
    pygame.display.update()
    clock.tick(62) #speed of the loop while frequency of repetition
    
