import tkinter as x
from tkinter import filedialog
import random

window = x.Tk()
frame = x.Frame()
x.Label(frame, text = "Wpisz słowa do wiersza, rozdziel je przecinkami").pack()
frame.pack()
frame_with_user_words = x.Frame()

lbl_noun = x.Label(frame_with_user_words, text="Rzeczowniki:")
lbl_verb = x.Label(frame_with_user_words, text="Czasowniki:")
lbl_adj = x.Label(frame_with_user_words, text="Przymiotniki:")
lbl_prep = x.Label(frame_with_user_words, text="Przyimki:")
lbl_adv = x.Label(frame_with_user_words, text="Przysłówki:")
ent_noun = x.Entry(frame_with_user_words, width=50)
ent_verb = x.Entry(frame_with_user_words, width=50)
ent_adj = x.Entry(frame_with_user_words, width=50)
ent_prep = x.Entry(frame_with_user_words, width=50)
ent_adv = x.Entry(frame_with_user_words, width=50)
lbl_noun.grid(row=2, column=1)
ent_noun.grid(row=2, column=2)
lbl_verb.grid(row=3, column=1)
ent_verb.grid(row=3, column=2)
lbl_adj.grid(row=4, column=1)
ent_adj.grid(row=4, column=2)
lbl_prep.grid(row=5, column=1)
ent_prep.grid(row=5, column=2)
lbl_adv.grid(row=6, column=1)
ent_adv.grid(row=6, column=2)
frame_with_user_words.pack()
frm_poet = x.Frame(master=window)
frm_poet.pack()
btn_poet = x.Button(frm_poet, text="Stwórz wiersz")
btn_poet.pack()
result_frame = x.Frame(master=window, relief = x.SUNKEN, borderwidth = 6)
result_label = x.Label(master=result_frame, text = "Twój wiersz:").pack(pady=8)
result_poem = x.Label(result_frame)
result_poem["text"] = ""
result_poem.pack(padx=7)
save_button = x.Button(result_frame, text="Zapisz plik z wierszem")
save_button.pack(pady=14)
result_frame.pack(fill=x.X, padx=5, pady=5)

def are_unique(word_list):
    unique_words = []
    for word in word_list:
        if word not in unique_words:
            unique_words.append(word)
    return len(word_list) == len(unique_words)

def generate_poem():
    noun = ent_noun.get().split(",")
    verb = ent_verb.get().split(",")
    adjective = ent_adj.get().split(",")
    adverb = ent_adv.get().split(",")
    preposition = ent_prep.get().split(",")
    if not (are_unique(noun) and are_unique(verb) and are_unique(adjective) and are_unique(adverb) and are_unique(preposition)):
        result_poem["text"] = "Wpisałeś dwukrotnie to samo słowo."
        return
    noun1 = random.choice(noun)
    noun2 = random.choice(noun)
    noun3 = random.choice(noun)
    while noun1 == noun2:
        noun2 = random.choice(noun)
    while noun1 == noun3 or noun2 == noun3:
        noun3 = random.choice(noun)
    verb1 = random.choice(verb)
    verb2 = random.choice(verb)
    verb3 = random.choice(verb)
    while verb1 == verb2:
        verb2 = random.choice(verb)
    while verb1 == verb3 or verb2 == verb3:
        verb3 = random.choice(verb)
    adj1 = random.choice(adjective)
    adj2 = random.choice(adjective)
    adj3 = random.choice(adjective)
    while adj1 == adj2:
        adj2 = random.choice(adjective)
    while adj1 == adj3 or adj2 == adj3:
        adj3 = random.choice(adjective)
    prep1 = random.choice(preposition)
    prep2 = random.choice(preposition)
    while prep1 == prep2:
        prep2 = random.choice(preposition)
    adv1 = random.choice(adverb)
    poem = f"{adj1} {noun1}\n\n"
    poem = poem + f"{adj1} {noun1} {verb1} {prep1} {adj2} {noun2}\n"
    poem = poem + f"{adv1}, {noun1} {verb2}\n"
    poem = poem + f"the {noun2} {verb3} {prep2} {adj3} {noun3}"
    result_poem["text"] = poem


def save_poem_to_file():
    type_list = [("Text files", "*.txt")]
    file_name = filedialog.asksaveasfilename(filetypes=type_list, defaultextension="*.txt")
    if file_name != "":
        with open(file_name, "w") as output_file:
            output_file.writelines(result_poem["text"])

btn_poet["command"] = generate_poem
save_button["command"] = save_poem_to_file
window.mainloop()
