
noun = [
    "fossil",
    "horse",
    "aardvark",
    "judge",
    "chef",
    "mango",
    "extrovert",
    "gorilla",
]
verb = [
    "kicks",
    "jingles",
    "bounces",
    "slurps",
    "meows",
    "explodes",
    "curdles",
]
adjective = [
    "furry",
    "balding",
    "incredulous",
    "fragrant",
    "exuberant",
    "glistening",
]
preposition = [
    "against",
    "after",
    "into",
    "beneath",
    "upon",
    "for",
    "in",
    "like",
    "over",
    "within",
]
adverb = [
    "curiously",
    "extravagantly",
    "tantalizingly",
    "furiously",
    "sensuously",
]

import random

print(f"\
      A {random.choice(adjective)} {random.choice(noun)}\n\n\
      A {random.choice(adjective)} {random.choice(noun)} {random.choice(verb)} {random.choice(preposition)} the  {random.choice(adjective)} {random.choice(noun)}\n\
      {random.choice(adverb)}, the {random.choice(noun)} {random.choice(verb)}\n\
      the {random.choice(noun)} {random.choice(verb)} {random.choice(preposition)} a {random.choice(adjective)} {random.choice(noun)}")

