#Computer plays with white pawns - NUMBERS, user with black ones - LETTERS

import copy
from math import fabs

name_of_white_to_capture = []
elements_to_possible_capture = []
final_position_after_capturing_list = []
elements_to_capture = []
board = [[8, 2, True, "black", "| A |"], [8, 4, True, "black", "| B |"], [8, 6, True, "black", "| C |"], [8, 8, True, "black", "| D |"],
        [7, 1, True, "black", "| E |"], [7, 3, True, "black", "| F |"], [7, 5, True, "black", "| G |"], [7, 7, True, "black", "| H |"],
        [6, 2, True, "black", "| I |"], [6, 4, True, "black", "| J |"], [6, 6, True, "black", "| K |"], [6, 8, True, "black", "| L |"],
        [5, 1, False, None, "|   |"], [5, 3, False, None, "|   |"], [5, 5, False, None, "|   |"], [5, 7, False, None, "|   |"], 
        [4, 2, False, None, "|   |"], [4, 4, False, None, "|   |"], [4, 6, False, None, "|   |"], [4, 8, False, None, "|   |"],
        [3, 1, True, "white", "| 9 |"], [3, 3, True, "white", "| 10|"], [3, 5, True, "white", "| 11|"],[3, 7, True, "white", "| 12|"], 
        [2, 2, True, "white", "| 5 |"], [2, 4, True, "white", "| 6 |"], [2, 6, True, "white", "| 7 |"], [2, 8, True, "white", "| 8 |"],
        [1, 1, True, "white", "| 1 |"], [1, 3, True, "white", "| 2 |"], [1, 5, True, "white", "| 3 |"], [1, 7, True, "white", "| 4 |"]]
class Pawn:
    def __init__(self, name, color, position, is_alive = True, is_queen = False):
        self.name = name
        self.color = color       
        self.position = position
        self.is_alive = is_alive
        self.is_queen = is_queen

def draw_board(): #prints the board
    raw_9 = ["   1  ", "  2  ", "  3  ", "  4  ", "  5  ", "  6  ", "  7  ","  8  "]
    raw_8 = ["8", "|---|", board[0][4], "|---|", board[1][4], "|---|", board[2][4], "|---|", board[3][4]]
    raw_7 = ["7",board[4][4], "|---|", board[5][4], "|---|", board[6][4], "|---|", board[7][4], "|---|"]
    raw_6 = ["6","|---|", board[8][4], "|---|", board[9][4], "|---|", board[10][4], "|---|", board[11][4]]
    raw_5 = ["5",board[12][4], "|---|", board[13][4], "|---|", board[14][4], "|---|", board[15][4], "|---|"]
    raw_4 = ["4","|---|", board[16][4], "|---|", board[17][4], "|---|", board[18][4], "|---|", board[19][4]]
    raw_3 = ["3",board[20][4], "|---|", board[21][4], "|---|", board[22][4], "|---|", board[23][4], "|---|"]
    raw_2 = ["2","|---|", board[24][4], "|---|", board[25][4], "|---|", board[26][4], "|---|", board[27][4]]
    raw_1 = ["1",board[28][4], "|---|", board[29][4], "|---|", board[30][4], "|---|", board[31][4], "|---|"]
    raws = [raw_9,raw_8, raw_7, raw_6, raw_5, raw_4, raw_3, raw_2, raw_1]
    for raw in raws:
        print(''.join(raw))
    return

pawns_black = [Pawn("| A |", "black", [8,2], True, False), Pawn("| B |", "black", [8,4], True, False),
               Pawn("| C |", "black", [8,6], True, False), Pawn("| D |", "black", [8,8], True, False),
               Pawn("| E |", "black", [7,1], True, False), Pawn("| F |", "black", [7,3], True, False),
               Pawn("| G |", "black", [7,5], True, False), Pawn("| H |", "black", [7,7], True, False),
               Pawn("| I |", "black", [6,2], True, False), Pawn("| J |", "black", [6,4], True, False),
               Pawn("| K |", "black", [6,6], True, False), Pawn("| L |", "black", [6,8], True, False)]

pawns_white = [Pawn("| 1 |", "white", [1,1], True, False), Pawn("| 2 |", "white", [1,3], True, False),
               Pawn("| 3 |", "white", [1,5], True, False), Pawn("| 4 |", "white", [1,7], True, False),
               Pawn("| 5 |", "white", [2,2], True, False), Pawn("| 6 |", "white", [2,4], True, False),
               Pawn("| 7 |", "white", [2,6], True, False), Pawn("| 8 |", "white", [2,8], True, False),
               Pawn("| 9 |", "white", [3,1], True, False), Pawn("| 10|", "white", [3,3], True, False),
               Pawn("| 11|", "white", [3,5], True, False), Pawn("| 12|", "white", [3,7], True, False)]
draw_board()

def pawn_white_move():
  
    def pawn_taken_decision(): #identyfing the white pawn with highest rate of posiible black pawns to be captured
        white_pawns_copy = copy.deepcopy(pawns_white)
        possible_attack = []
        for a in white_pawns_copy: 
            if a.is_alive == True and a.is_queen == False:
                for b in board_copy: # checks is there any enemy's pawn next to
                    if all ([b[0] == a.position[0] + 1, b[1] == a.position[1] + 1, b[2] == True, b[3] == "black"]) or\
                       all ([b[0] == a.position[0] + 1, b[1] == a.position[1] - 1, b[2] == True, b[3] == "black"]) or\
                       all ([b[0] == a.position[0] - 1, b[1] == a.position[1] - 1, b[2] == True, b[3] == "black"]) or\
                       all ([b[0] == a.position[0] - 1, b[1] == a.position[1] + 1, b[2] == True, b[3] == "black"]) :
                        b[2] = False #allows no to check all the time the same enemy's pawn (prevents moves there and back)
                        for c in board: #checks if the position on board behind the enemy's pawn is empty, that allows the pawn to be captured
                            if all ([b[0] < a.position[0], b[1] > a.position[1], c[0] == a.position[0] - 2 , c[1] == a.position[1] + 2, c[4] == "|   |"]) or \
                               all ([b[0] < a.position[0], b[1] < a.position[1], c[0] == a.position[0] - 2 , c[1] == a.position[1] - 2, c[4] == "|   |"]) or \
                               all ([b[0] > a.position[0], b[1] < a.position[1], c[0] == a.position[0] + 2 , c[1] == a.position[1] - 2, c[4] == "|   |"]) or \
                               all ([b[0] > a.position[0], b[1] > a.position[1], c[0] == a.position[0] + 2 , c[1] == a.position[1] + 2, c[4] == "|   |"]):
                                possible_attack.append(Pawn(a.name, a.color, [c[0],c[1]])) #collection of white pawns, and the position they move during the capturing an enemy's pawn
                                name_of_white_to_capture.append(a.name) #collection of white pawns which are able to capture enemy's pawn
                                elements_to_possible_capture.append([a.name, b[0], b[1], b[4]]) #collection of black pawns - their positions, which can be captured by white pawns
                                final_position_after_capturing_list.append([a.name, c[0], c[1]])
                                if len(final_position_after_capturing_list) > 1:
                                    if fabs(final_position_after_capturing_list[-1][2] - final_position_after_capturing_list[-2][2]) > 2 or\
                                       fabs(final_position_after_capturing_list[-1][1] - final_position_after_capturing_list[-2][1]) > 2: #considers only the longest posiible move of capturing
                                        del elements_to_possible_capture[-2]
                                        del final_position_after_capturing_list[-2]
                            else:
                                pass
                    else:
                        pass
            elif a.is_alive == True and a.is_queen == True:# TU PRACUJESZ TERAZ/ 10 jest damka
                for b in board_copy: # checks is there any enemy's pawn next to
                    if all ([b[2] == True, b[3] == "black", fabs(b[0]-a.position[0]) == fabs(b[1] - a.position[1])]):
                        for c in board:
                            if all ([c[0] == b[0] + 1, c[1] == b[1] + 1 , c[2] == False]): #check is there free place behind selected enemy's pawn
                                f = 1
                                for c in board:
                                    if all([b[0] - f == c[0], b[1] - f == c[1], c[2] == False or c[2] == a.name]):
                                        
                                        if c[2] == a.name:
                                            break
                                        else:
                                            f += 1
                        if fabs(f) == fabs(b[0] - a.position[0]):
                            
                            possible_attack.append(Pawn(a.name, a.color, [c[0],c[1]])) #collection of white pawns, and the position they move during the capturing an enemy's pawn
                            name_of_white_to_capture.append(a.name) #collection of white pawns which are able to capture enemy's pawn
                            elements_to_possible_capture.append([a.name, b[0], b[1], b[4]]) #collection of black pawns - their positions, which can be captured by white pawns
                            final_position_after_capturing_list.append([a.name, c[0], c[1]])
                            if len(final_position_after_capturing_list) > 1:
                                if fabs(final_position_after_capturing_list[-1][2] - final_position_after_capturing_list[-2][2]) > 2 or\
                                    fabs(final_position_after_capturing_list[-1][1] - final_position_after_capturing_list[-2][1]) > 2: #considers only the longest posiible move of capturing
                                    del elements_to_possible_capture[-2]
                                    del final_position_after_capturing_list[-2]
           
        return possible_attack
    
    def move_forward(): #finds the first pawn which can move - has free place in front of it, and moves there
        is_pawn_found = False
        for a in pawns_white:
            if a.is_alive == True and is_pawn_found == False:
                for b in board: # checks is there any enemy's pawn next to
                    if all ([b[0] == a.position[0] + 1, b[1] == a.position[1] + 1, b[2] == False]) or\
                       all ([b[0] == a.position[0] + 1, b[1] == a.position[1] - 1, b[2] == False]):
                        for d in board: #set the pawn to new position on board
                            if d[0] == b[0] and d[1] == b[1]:
                                d[4] = a.name
                                d[3] = a.color
                                d[2] = True
                            else:
                                pass
                        for d in board: #cleans the place on board left by white pawn
                            if d[0] == a.position[0] and d[1] == a.position[1]:
                                d[4] = "|   |"
                                d[3] = None
                                d[2] = False
                            else:
                                pass
                        for d in pawns_white: # change the position parameters in the pawn which moved
                            if d.name == a.name:
                                name_pawn_to_move = d.name
                                d.position[0] = b[0]
                                d.position[1] = b[1]
                            else:
                                pass
                        is_pawn_found = True
                        break
        return name_pawn_to_move
    
    number_combination = 0
    for y in range(1,13): #proceeds pawn_taken_decision for all the pawn - max 12
        white_pawns_copy = copy.deepcopy(pawns_white[(y-1):y])
        board_copy = copy.deepcopy(board)
        while len(white_pawns_copy) != 0: 
            white_pawns_copy = pawn_taken_decision()
            number_combination = number_combination + 1 #if only 12 iterations (12 pawns) means there is no capturing opportunity found
    
    if number_combination > 12:
        pawn_to_make_capture_list = []       
        for a in name_of_white_to_capture: #selects white pawn which has most posiibilities to capture enemy's pawn
            pawn_to_make_capture_list.append([name_of_white_to_capture.count(a), a])
            pawn_to_make_capture_list.sort()
            
            pawn_to_make_capture = pawn_to_make_capture_list[-1][1]
        
        for l in elements_to_possible_capture: #removes captured pawns out of board, and make inactive in enemy's pawn collection
            if l[0] == pawn_to_make_capture:
                elements_to_capture.append([l[1],l[2]])
                for e in board:
                    if all([ l[1] == e[0], l[2] == e[1] ]):
                        e[4] = "|   |"
                        e[3] = None
                        e[2] = False
                        for i in pawns_black:
                            if i.name == l[3]: 
                                i.is_alive = False
                    else:
                        pass
    
        for f in final_position_after_capturing_list: #determinates final position of white pawn after making capturing
            if f[0] == pawn_to_make_capture:
                final_position_after_capturing = [f[1],f[2]]

        for w in pawns_white:
            if w.name == pawn_to_make_capture:
                w.position = final_position_after_capturing
    
        for a in board: # erase the place on board held by white pawn which made the capture
            if a[4] == pawn_to_make_capture:
                a[4] = "|   |"
                a[3] = None
                a[2] = False

        for a in board: # put the white pawn which made the capture on new position
            if a[0] == final_position_after_capturing[0] and a[1] == final_position_after_capturing[1]:
                a[4] = pawn_to_make_capture
                a[3] = "white"
                a[2] = True
    else:
        pawn_to_make_capture = f"Brak możliwego bicia, wykonano ruch pionem{move_forward()}" # none of white pawn has opportunity to capture the black one
        
    return pawn_to_make_capture

def pawn_black_move(): #move the black pawn - users pawn
    pawn_black_to_move = input("Graczu podaj nazwę piona, którym się ruszasz: ").upper()
    pawn_black_place_to_move = input("Graczu podaj wspołrzędne pola gdzie się ruszasz wiersz, rząd: ")
    white_pawns_captured_number = int(input("Graczu podaj ilość pionów rywala, które zbiłeś: "))
    if white_pawns_captured_number == 0:
        pass
    else:
        for i in range(0,white_pawns_captured_number):
            white_pawns_captured = int(input("Podaj nazwę zbitego piona:"))
            if white_pawns_captured  < 10:
                for k in board: #removes captured pawns form board
                    if k[4] == f"| {white_pawns_captured} |":
                        k[4] = "|   |"
                        k[3] = None
                        k[2] = False
                    else:
                        pass
                for l in pawns_white:
                    if l.name == f"| {white_pawns_captured} |":
                        l.is_alive = False
                    else:
                        pass
            else:
                for k in board: #removes captured pawns from avaiable ones for the opponent
                    if k[4] == f"| {white_pawns_captured}|":
                        k[4] = "|   |"
                        k[3] = None
                        k[2] = False
                    else:
                        pass
                for l in pawns_white:
                    if l.name == f"| {white_pawns_captured}|":
                        l.is_alive = False
                        
                    else:
                        pass
        
    for i in pawns_black: #change position of the pawn selected to move into the new one
        if i.is_alive == True and i.name == f"| {pawn_black_to_move} |" :
            for j in board: #put the pawn in new place on board
                if j[0] == int(pawn_black_place_to_move[0]) and j[1] == int(pawn_black_place_to_move[2]):
                    j[2] = True
                    j[3] = "black"
                    j[4] = i.name
            for k in board: #remove pawn from previous place of board
                if k[0] == i.position[0] and k[1] == i.position[1]:
                    k[2] = False
                    k[3] = None
                    k[4] = "|   |"
            i.position = [int(pawn_black_place_to_move[0]), int(pawn_black_place_to_move[2])] #change the current position into new one in pawns collection
        else:
            pass
    return 

is_any_white_pawns_alive = True
is_any_black_pawns_alive = True
number_of_death_pawns_white = 0
number_of_death_pawns_black = 0

def queen(): #checks if any pawn reached teh area to changing into queen. If yes - change its atrritbute into queen.
    for i in board:
        if i[0] == 8 and i[3] == "white":
            for a in pawns_white:
                if a.name == i[4]:
                    a.is_queen = True
                    a.name = "| 0 |"
                    i[4] = "| 0 |"
            
        elif i[0] == 1 and i[3] == "black": # change the name on board and prgram into quenn. 
            for a in pawns_black:
                if a.name == i[4]:
                    a.is_queen = True
                    a.name = "| Q |"
                    i[4] = "| Q |"
                   
        else:
            pass

while is_any_white_pawns_alive == True and is_any_black_pawns_alive == True:
       
    number_of_death_pawns_white = 0
    for i in pawns_white:
        
        if i.is_alive == True:
            
            print(f"KOMPUTER: bije pionem nr {pawn_white_move()}")
            break
        else:
            number_of_death_pawns_white += 1
            if number_of_death_pawns_white == 12:
                print("KONIEC GRY, WYGRAŁEŚ!!!")
                is_any_white_pawns_alive = False
    draw_board()          
    if is_any_white_pawns_alive == True:
        for i in pawns_black:
            if i.is_alive == True:
                pawn_black_move()
                break
            else:
                number_of_death_pawns_white += 1
                if number_of_death_pawns_white == 12:
                    print("KONIEC GRY, PRZEGRAŁEŚ!!!")
                    is_any_white_pawns_alive = False
    else:
        pass
    queen()
    draw_board()
       
