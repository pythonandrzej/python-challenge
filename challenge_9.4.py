import statistics

def enrollment_stats(university_data):
        
    for i in university_data:
       
        students_enrollment.append(i[1])
        tutation_fees.append(i[2])

    return students_enrollment, tutation_fees



def mean(list_to_be_meaned):
    return statistics.mean(list_to_be_meaned)



def median(list_to_be_median):
    return statistics.median(list_to_be_median)


universities = [
    ["California Institiute of Technology", 2175, 37704],
    ["Harvard", 19627, 39849],
    ["Massachustts Institute of Technology", 10566, 40732],
    ["Princteon", 7802, 37000],
    ["Rice", 5879, 35551],
    ["Stanford", 19535, 40569],
    ["Yale", 11701, 40500]
]

students_enrollment = []
tutation_fees = []
enrollment_stats(universities)

print(f"*****************************\n\
Total students: {sum(students_enrollment)}\n\
Total tuition: {sum(tutation_fees)}\n\n\n\
Student mean: {mean(students_enrollment):.2f}\n\
Tutition mean: {mean(tutation_fees):.2f}\n\n\n\
Student median: {median(students_enrollment)}\n\
Student median: {median(tutation_fees)}\n\
*****************************")
