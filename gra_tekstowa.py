print(f"Witaj Biegaczu! Przed Tobą bieg! Pokaż co potrafisz.\nUkończ bieg w jak najkrótszym czasie")
player_name = input("Podaj swoje imię: ")

import time
import random

wheater = ["słoneczna", "deszczowa", "śnieżna"]

class Shoes:
    def __init__(self, brand, type_of_sole, size, wheater_condition = ""):
        self.brand = brand
        self.type_of_sole = type_of_sole
        self.size = size
        self.wheater_condition = random.choice(wheater)

    def difficulty(self):
        if shoe.wheater_condition != "słoneczna":
            if shoe.type_of_sole == "1":
                run_cadence = 1
            elif shoe.type_of_sole == "2":
                run_cadence = 2
            else:
                run_cadence = 10
        else:
            run_cadence = 1
        return run_cadence
        
class Drink:
    def __init__(self, type_of_drink):
        self.type_of_drink = type_of_drink

    def power_of_hydration(self):
        if int(drink.type_of_drink) == 1:
            run_cadence = 0.2
        elif int(drink.type_of_drink) == 2:
            run_cadence = 0.5
        else:
            run_cadence = 1
        return run_cadence

def bugdet_decrease(product_number):
    if product_number == 1:
       price = 700
    elif product_number == 2:
        price = 300
    else:
        price = 100
    return price

print("Twój budżet to 1000 PLN. Wybierz buty i napój")

while 1 > 0:
    
    shoe = Shoes(input("Wpisz nazwę wybranej marki: Nike, Puma lub Adidas "), input("Wpisz cyfrę odpowiednią dla podeszwy, na którą się decydujesz: \n1- GÓRSKA, cena: 700 PLN,\
    uratuje Cię na każdej powierzchni \n2- NORMALNA - 300 PLN świetna na suchą, ale słaba na każdą inną,\n3- PŁASKA - 100 PLN, niemożliwy bieg po śliskiej nawierzchni.\
    Kontuzjogenna, ale dobra na suchą nawierzchnię: "),input("Podaj rozmiar: "))

    print(f"Pozostało Ci {1000 - bugdet_decrease(int(shoe.type_of_sole))} PLN")

    time.sleep(1)
    drink = Drink(input("Wpisz cyfrę odpowiednią dla napoju jaki wybierasz:\n1 - ENERGETYK - 700 PLN - dodaje energii\n2 - WODA - 300 PLN - pozwala nie oslabnąć w trakcie biegu\n3 - ZGRZEWKA PIWA - 100 PLN -w trakcie biegu nie pomoże, ale dodaje otuchy na mecie"))
    if (bugdet_decrease(int(drink.type_of_drink)) + bugdet_decrease(int(shoe.type_of_sole))) > 1000:
        print("Przekroczyłeś budżet 1000 PLN. Zaczynasz zakupy od nowa")
        continue
    else:
        break

run_progress = 0
print(f"By biec wpisuj ciągle słowo 'run' na klawiaturze i wciska ENTER. STTTAAARRTTT!")
begin_of_run_time = time.time()
if shoe.difficulty() > 1:
    print("Twoje buty są nieodpowiednie do pogody, będziesz biegł wolniej, ale nie poddawaj się. Pogoda zmieni się na korzystą po 20% trasy")
else:
    pass
while run_progress < 15:
    run_command = input()
    
    if run_command == "run":
        run_progress += 4/shoe.difficulty()
        print(f"{run_progress}% trasy za Tobą")
    else:
        pass

print("Przed Tobą przeszkoda! Przewrócone drzewo! By je przeskoczyć musisz się solidnie rozpędzić! Osiagniesz to wpisując 'run' i klikając ENTER 8 razy w 6 sekund!\n STARRTT!!!!!")
while 1 > 0:
    acceleration_progress = 0
    start_time = time.time()
    while acceleration_progress < 8:
        run_command = input()
        if run_command == "run":
            acceleration_progress += 1
        else:
            pass
    end_time = time.time()
    
    if end_time - start_time <= 6:
        print("Brawo! Biegnij dalej wpisując 'run' i ENTER")
        break
    else:
        print(f"Za wolny rozbieg. Wpisanie 8 razy run zajęło Ci {end_time - start_time:.3} sekund. Próbuj ponownie!\n START!")
        continue
run_progress = 40
while run_progress < 50:
    run_command = input()
    
    if run_command == "run":
        run_progress += 4/shoe.difficulty()
        print(f"{run_progress}% trasy za Tobą")
    else:
        pass
print(f"Połowa trasy za Tobą. Wychodzi słońce. Czas na napój, który zakupiłeś! Wypijasz go!")

if drink.power_of_hydration() == 0.2:
    print("Energetyk, który kupiłeś dodaje Ci sił - Twój jeden krok to realnie 5! Wpisuj 'run' i gnaj do mety!")
elif drink.power_of_hydration() == 0.5:
    print("Woda przywraca siły. Jeden krok daje Ci 2 razy większą odległość!Wpisuj 'run' i gnaj do mety!")
else:
    print("Piwko, które zakupiłeś nic Ci nie pomaga teraz, ale...pełznij dalej wciskając 'run', na mecie będzie smakować wybornie!")

while run_progress < 100:
    run_command = input()
    
    if run_command == "run":
        run_progress += 4/drink.power_of_hydration()
        print(f"{run_progress}% trasy za Tobą")
    else:
        pass
print(f"META! {player_name} twój czas biegu to {time.time()-begin_of_run_time:.3}sekund")
