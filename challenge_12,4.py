from pathlib import Path


folder = Path.home() / "Desktop/Python proba" 


folder_destination = Path.home() / "images"
folder_destination.mkdir(exist_ok=True)

for a in folder.rglob("*.*"):
    if a.suffix in [".png", ".jpg", ".gif", ".bmp"]:
        a.replace(folder_destination / a.name)
