def convert_cel_to_far(cel):
    far = cel * 9 / 5 + 32
    return far
def convert_far_to_cel(far):
    cel = (far - 32) * 5 / 9
    return cel 

far = float(input("Wpisz temperaturę w skali Fahrenheit'a: "))
print(f"Temp. {far:.2f} F = {convert_far_to_cel(far):.2f} C")
cel = float(input("Wpisz temperaturę w skali Celsjusza: "))
print(f"Temp. {cel:.2f} C = {convert_cel_to_far(cel):.2f} F")
