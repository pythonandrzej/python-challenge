class Animal:
    def __init__(self, name, spiece, group, is_for_slaughter):
        self.name = name
        self.spiece = spiece
        self.group = group
        self.is_for_slaughter = bool(is_for_slaughter)
        self.alive = True
    
    def talk(self, speak = None):
        if speak == None:
            print("Brak informaji o rodzaju wydawanego głosu")
        else:
            print(f"{self.name} mówi {speak}")

    def slaughter(self):
        if self.alive == True:
            self.alive = not self.alive
            print(f"zwierzę {self.name} właśnie poszła na kotlety")
        else:
            print(" i am already dead misu")
        return self.alive
        

class Dog(Animal):
    def sheep_lead(self, is_trained_for_sheeps = False):
        self.is_trained_for_sheeps = bool(is_trained_for_sheeps)


class Sheep(Animal):
    def slaughter(self):
        return super().slaughter()


class Pig(Animal):
    def __init__(self, name, spiece, group, is_for_slaughter, mass_of_body):
        self.mass_of_body = mass_of_body
        super().__init__(name, spiece, group, is_for_slaughter)
        
    def slaughter(self):
       if self.mass_of_body < 30:
            print(f"{self.spiece} o imieniu {self.name} nie osiagnęła jeszcze masy odpowiedniej")
       else:
        return super().slaughter()
    

