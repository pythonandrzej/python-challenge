import random

Candidate_A = 0
Candidate_B = 0

def voting(probability_win_A):
    if random.random() < probability_win_A:
        return "Candidate A"
    else:
        return "Candidate B"

for i in range(10_000):
    
    if voting(.87) == "Candidate A" and voting(.65) == "Candidate A" \
       or voting(.17) == "Candidate A" and voting(.65) == "Candidate A" \
       or voting(.87) == "Candidate A" and voting(.17) == "Candidate A": 
        Candidate_A = Candidate_A + 1
    else:
        Candidate_B = Candidate_B + 1

print(f"Probability Candidate A wins: {Candidate_A * 100 / 10000: .0f} %")
print(f"Probability Candidate B wins: {Candidate_B * 100 / 10000: .0f} %")
