import easygui as x
from PyPDF2 import PdfFileReader, PdfFileWriter

path = x.fileopenbox("", "Wybierz plik, z którego wytniemy strony", "*.pdf")
if path is None:
    exit()
begin_page = x.enterbox("Wprowadź stronę początkową", "Wprowadź stronę początkową")
PDF_file = PdfFileReader(path)  
pages = PDF_file.getNumPages()

if begin_page is None:
    exit()
while any([begin_page == "0", int(begin_page) > pages, not begin_page.isdigit()]):
    x.msgbox("Podaj poprawny numer strony", "Niepoprawny numer strony")
    begin_page = x.enterbox("Wprowadź stronę początkową", "Wprowadź stronę początkową")
    if begin_page is None:  
        exit()

last_page = x.enterbox("Wprowadź ostatnią stronę", "Wprowadź ostatnią stronę")
if last_page is None:
    exit()
while any([not last_page.isdigit(), int(last_page) > pages, int(last_page) < int(begin_page)]):
    x.msgbox("Podaj poprawny numer strony", "Niepoprawny numer strony")
    last_page = x.enterbox("Wprowadź ostatnią stronę", "Wprowadź ostatnią stronę")
    if last_page is None:
        exit()

destination_path = x.filesavebox("", "Zapisz nowy plik jako", "*.pdf")
if destination_path is None:
    exit()
while path == destination_path: 
    x.msgbox("Probujesz zapisać plik po zmianach na miejscu orygnalnego.", "Niedozwolona operacja")
    destination_path = x.filesavebox("", "Zapisz nowy plik jako", "*.pdf")
    if destination_path is None:
        exit()

output_PDF = PdfFileWriter()
for i in range(int(begin_page) - 1, int(last_page)):
    page = PDF_file.getPage(i)
    output_PDF.addPage(page)
with open(destination_path, "wb") as file_to_create:
    output_PDF.write(file_to_create)
